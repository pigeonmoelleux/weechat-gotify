# Weechat-Gotify

This script is a weechat python script made to send a push notification with gotify each time

## Pre-installation

To install it, you need two thing beforehand :
 * a running instance of [Gotify](https://gotify.net/) ;
 * a configured instance of [gotify/cli](https://github.com/gotify/cli).

## Installation

Create a `.env` file as following :

```
GOTIFY_PATH		= PATH_TO_GOTIFY_CLI
USER_NICK 		= USERNAME
```

Then, place `gotify.py` and `.env` in the folder `~/.weechat/python/` (or where your weechat config is), then run the following command in weechat :

```
/python load python/gotify.py
```
from os import getenv
from dotenv import load_dotenv
import subprocess

import weechat

load_dotenv()

SCRIPT_NAME    	= "gotify_ping"
SCRIPT_AUTHOR  	= "Pigeon Moelleux <pigeonmoelleux@crans.org>"
SCRIPT_VERSION 	= "1.0.1"
SCRIPT_LICENSE 	= "GPL3"
SCRIPT_DESC    	= "Send a notification via gotify at each ping"

GOTIFY_PATH		= getenv("GOTIFY_PATH")
USER_NICK 		= getenv("USER_NICK")

settings = {
	"lang" : "en"
}

def detect_ping(data, signal, signal_data):
	server, _ = signal.split(",")
	msg = weechat.info_get_hashtable("irc_message_parse", {"message" : signal_data})
	buffer = weechat.info_get("irc_buffer", "%s,%s" % (server, msg["channel"]))
		
	msg_text = ":".join(signal_data.split(":")[2:])
	try:
		channel = "#" + signal_data.split("#")[1].split(" ")[0]
	except:
		channel = "prvtmsg"

	if USER_NICK in msg_text or channel == "prvtmsg":
		dev_null = open("/dev/null", "w")
		subprocess.run([GOTIFY_PATH, "push", channel + "@" + signal_data[1:].split("!")[0] + " : " + msg_text], stdout=dev_null)
		dev_null.close()

	return weechat.WEECHAT_RC_OK
	

if weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION, SCRIPT_LICENSE, SCRIPT_DESC, "", ""):
	weechat.hook_signal("*,irc_in2_privmsg", "detect_ping", "")